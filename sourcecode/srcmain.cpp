//Standard Headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
using namespace glm;

int main(void )
{
    //init GLFW  -- the window
    if(!glfwInit())
    {
        fprintf(stderr, "Failed to init GLFW\n");
        getchar();
        return -1;
    }else{
        printf("GL works");
    }

    //gl settings
    glfwWindowHint(GLFW_SAMPLES, 4);//antialiasing 4x
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //opengl 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //denied the old opengl

    //opengl context on a window
    window = glfwCreateWindow(1280, 720, "GL Scene", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    
    //init GLEW -- the GL
    if (glewInit() != GLEW_OK)
    {
        fprintf(stderr, "GLEW failed");
        getchar();
        glfwTerminate();
        return -1;
    }

    //capture the escape key
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    //bg Color
    glClearColor(0.05f, 0.05f, 0.1, 0.0f);

    //at this moment, the windows will open and close almost immediately,
    //that's why is necessary to establish a loop, the game loop
    //the loop will redraw every frame the screen
    do{
        //avoid flickering - TO DO search about
        glClear(GL_COLOR_BUFFER_BIT);

        //draw nothing

        //swap buffers - TO DO  search about
        glfwSwapBuffers(window); //swap buffers in the window
        glfwPollEvents(); //queue of events will be executed

    }//this is a do-while
    while(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
            glfwWindowShouldClose(window) == 0);

    //close opengl and the glfw window
    glfwTerminate();

    return 0;

}
